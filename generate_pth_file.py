#!/usr/bin/python

import pathlib as pl


def generate_package_path_file(package_name: str, output_folder: pl.Path):
    output_folder = output_folder if isinstance(output_folder, pl.Path) else pl.Path(output_folder)
    current_folder = pl.Path(__file__).parent
    output_file = output_folder / f"{package_name}.pth"
    with open(output_file, "w") as f:
        f.write(str(current_folder))
    return output_file


if __name__ == "__main__":
    generate_package_path_file("graph_toolkit", pl.Path(__file__).parent / "dist")
