from typing import Optional
from abc import abstractmethod
from dataclasses import dataclass, field
import copy
import itertools


@dataclass
class GraphNode:
    def __post_init__(self) -> None:
        self._id: str = self.calculate_id()
        self._owner: Graph = None
        self._inputs: list[GraphEdge] = []
        self._outputs: list[GraphEdge] = []
    
    @property
    def inputs(self) -> list['GraphEdge']:
        return self._inputs
    
    @property
    def outputs(self) -> list['GraphEdge']:
        return self._outputs
    
    def __str__(self) -> str:
        return self.to_str()
    
    @property
    def id(self):
        return self._id
    
    @property
    def owner_graph(self):
        return self._owner
    
    @owner_graph.setter
    def owner_graph(self, value):
        self._owner = value
    
    def set_id(self, value):
        self._id = value
    
    @abstractmethod
    def calculate_id(self) -> str:
        pass
    
    @abstractmethod
    def to_str_debug(self):
        pass
    
    @abstractmethod
    def to_str(self):
        pass
    
    @abstractmethod
    def merged(self, other):
        """Returns new node without modifying original nodes"""
        pass


class GraphEdge:
    
    def __init__(self, node_from: GraphNode, node_to: GraphNode) -> None:
        self._node_from = None
        self._node_to = None
        self.node_from = node_from
        self.node_to = node_to
    
    @property
    def node_from(self) -> GraphNode:
        return self._node_from
    
    @node_from.setter
    def node_from(self, new_node):
        assert isinstance(new_node, GraphNode)
        if self._node_from is not None:
            self._node_from.outputs.remove(self)
        new_node.outputs.append(self)
        self._node_from = new_node
    
    @property
    def node_to(self) -> GraphNode:
        return self._node_to
    
    @node_to.setter
    def node_to(self, new_node):
        assert isinstance(new_node, GraphNode)
        if self._node_to is not None:
            self._node_to.inputs.remove(self)
        new_node.inputs.append(self)
        self._node_to = new_node
    
    def __str__(self) -> str:
        return self.to_str()
    
    @abstractmethod
    def to_str(self):
        pass

    def disconnect(self):
        self.node_from = None
        self.node_to = None


class Graph:
    EdgeClass = GraphEdge
    
    def __init__(self) -> None:
        self._next_node_id: int = 0
        self.nodes: list[GraphNode] = []
        self.edges: list[GraphEdge] = []
    
    def add_node(self, node: GraphNode):
        if node.owner_graph is self:
            return node
        
        node.owner_graph = self
        node.set_id(self._next_node_id)
        self._next_node_id += 1
        self.nodes.append(node)
        return node
    
    def create_node(self, node_cls, *args, **kwargs):
        return self.add_node(node_cls(*args, **kwargs))

    def create_edge(self, node_from: GraphNode, node_to: GraphNode, *args, **kwargs):
        self.edges.append(self.EdgeClass(node_from, node_to, *args, **kwargs))
    
    def remove_node(self, node: GraphNode):
        self.remove_node_edges(node)
        self.nodes.remove(node)
    
    def remove_node_edges(self, node: GraphNode):
        unique_edges: list[GraphEdge] = []
        for e in itertools.chain(node.inputs, node.outputs):
            if e not in unique_edges:
                unique_edges.append(e)

        for edge in unique_edges:
            edge.disconnect()
            self.edges.remove(edge)
    
    def remove_edges(self, node_from: GraphNode, node_to: GraphNode):
        for edge in node_from.outputs:
            if edge in node_to.inputs:
                edge.disconnect()
                self.edges.remove(edge)
    
    def remove_subtree(self, node: GraphNode):
        # Removes all nodes starting from furthest leaves
        visited_nodes = []
        while node not in visited_nodes:
            leafs = self.get_subtree_leafs(node)
            visited_nodes.extend(leafs)
            for leaf in leafs:
                self.remove_node(leaf)
    
    def get_subtree_leafs(self, node: GraphNode, visited=None):
        if visited is None:
            visited = []
        
        if self.is_leaf_node(node):
            return node
        
        if node in visited:
            return
        visited.append(node)
        
        for inp in node.inputs:
            inp_node = inp.node_to
            if self.is_leaf_node(inp_node):
                yield inp_node
            else:
                self.get_subtree_leafs(inp_node)
    
    def replace_nodes(self, initial_nodes: list[GraphNode], target_node: GraphNode):
        """Replaces node and modifies all edges connected to it"""

        new_node = self.add_node(target_node)
        for node in initial_nodes:
            for inp_edge in node.inputs:
                inp_edge.node_to = new_node
            for out_edge in node.outputs:
                out_edge.node_from = new_node
            self.remove_node(node)
        return new_node
    
    def join_by_node(self, self_node: GraphNode, 
                     other: 'Graph', other_node: GraphNode, 
                     common_node: GraphNode):
        self._validate_graph(other)
        
        new_nodes = self.append_graph(other)
        self_node = new_nodes[self_node.id]
        other_node = new_nodes[other_node.id]
        common_node = self.add_node(common_node)
        self.replace_nodes([self_node, other_node], common_node)

    def append_graph(self, other: 'Graph'):
        self._validate_graph(other)
        
        other = copy.deepcopy(other)
        node_mapping = {}
        
        # transfers ownership of nodes to this graph
        for node in other.nodes:
            old_id = node.id
            new_node = self.add_node(node)
            node_mapping[old_id] = new_node
        
        # appends all edges with updated nodes
        for edge in other.edges:
            self.edges.append(edge)

        return node_mapping
    
    def find_sink_nodes(self) -> list[GraphNode]:
        return list(filter(lambda node: len(node.outputs) == 0, self.nodes))
    
    def get_node_inputs_outputs(self, nodes: list[GraphNode]):
        node_inputs_outputs: dict[int, tuple[list[GraphEdge], list[GraphEdge]]] = {}
        for rate_node in nodes:
            node_inputs_outputs[rate_node.id] = ([], [])

        for edge in self.edges:
            if edge.node_to.id in node_inputs_outputs:
                inp_out = node_inputs_outputs[edge.node_to.id]
                inp_out[0].append(edge)
            if edge.node_from.id in node_inputs_outputs:
                inp_out = node_inputs_outputs[edge.node_from.id]
                inp_out[1].append(edge)
        return node_inputs_outputs
    
    def _validate_graph(self, other):
        assert isinstance(other, self.__class__)
        assert self.EdgeClass == other.EdgeClass
    
    def is_leaf_node(self, node: GraphNode):
        for edge in self.edges:
            if edge.node_to is node:
                return False
        return True
    